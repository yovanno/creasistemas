﻿using Interfacez;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Repositorios
{
    public abstract class RepositorioBase<TContext, TEntity> : IRepositorioBase<TEntity>
        where TContext : DbContext, new() where TEntity : class
    {
        private TContext context = new TContext();

        public IEnumerable<TEntity> Get()
        {
            return this.context.Set<TEntity>();
        }

        public virtual bool Add(TEntity entity)
        {
            try
            {
                this.context.Set<TEntity>().Add(entity);
                this.context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}