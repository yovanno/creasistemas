import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { url } from 'inspector';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public forecasts: Comics[];
  public comicsenti: Comics;
  public client: HttpClient;
  public url: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
    this.client = http;
    http.get<Comics[]>('http://gateway.marvel.com/v1/public/characters?ts=1&apikey=8434f423edf2203916fb7b4b08bdf15c&hash=f78d3557f68bf6faab0f18129dd90051&limit=6').subscribe(result => {
      this.forecasts = result.data.results;
      console.log(result.data.results);
    }, error => console.error(error));
  }

  public EnviarInformacion(datacomict: Comics, Accion: string) {
    datacomict.Usuario = localStorage.getItem('user');
    datacomict.Accion = Accion;
    console.log(this.url);

    this.client.post<Comics>(this.url + 'api/Comics/GuardarInformacion', datacomict).subscribe(result => {
      console.log(result);
    }, error => console.error(error));
  }
  
}

interface Comics {
  Accion: string;
  Usuario: string;
  name: string;
}
