import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  public currentCount = 0;
  public contact = { name: '', email: '' };
  constructor(
    private router: Router
  ) { }

  public EnviarInformacion()
  {
    localStorage.setItem('user', this.contact.name);
    console.log(this.contact.name + '' + this.contact.email);
    this.router.navigate(['/fetch-data'])
  }
}
