using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dao.Models;
using Entidades;
using Interfacez;
using Microsoft.AspNetCore.Mvc;

namespace SpaAngular.Controllers
{
    [Route("api/[controller]")]
    public class ComicsController : Controller
    {
        private IRepositorioComics _repositorioComics;
        public ComicsController(IRepositorioComics repositorioComics)
        {
            _repositorioComics = repositorioComics;
        }
        [HttpPost("[action]")]
        public ActionResult GuardarInformacion([FromBody] ComicsDTO comics)
        {
            Comics comic = new Comics();
            comic.Accion = comics.Accion;
            comic.Nombre = comics.Name;
            comic.Usuario = comics.Usuario;
            return Ok(_repositorioComics.Add(comic));
        }
    }
}