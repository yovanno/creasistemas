﻿using System;

namespace Entidades
{
    public class ComicsDTO
    {
        /// <summary>
        /// get y set Accion
        /// </summary>
        public string Accion { get; set; }

        /// <summary>
        /// get y set Usuario
        /// </summary>
        public string Usuario { get; set; }

        /// <summary>
        /// get y set Personaje
        /// </summary>
        public string Name { get; set; }
    }
}
