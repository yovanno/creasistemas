﻿using System;
using System.Collections.Generic;

namespace Entidades
{
    public partial class Comics
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Accion { get; set; }
        public string Usuario { get; set; }
    }
}
