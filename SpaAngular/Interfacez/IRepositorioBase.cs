﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfacez
{
    public interface IRepositorioBase<TEntity>
    {
        bool Add(TEntity entity);
        IEnumerable<TEntity> Get();
    }
}