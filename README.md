# CreaSistemas

Prueba Técnica CreaSistemas Angular y .Net Core

Para ejecutar el proyecto se debe tener instalado .Net Core 2.2 y Node.Js 8.1 o superior, adicional se debe ejecutar el script que se encuentra en la carpeta Scripts del proyecto DAO o ejecutar los siguientes scripts en la consola NPG 

- dotnet ef migrations add CreateSchoolDB

- dotnet ef database update

desde el proyecto DAO para crear la base de datos DBCore que se utiliza para registrar los dato0s obtenidos desde la aplicación SPA.